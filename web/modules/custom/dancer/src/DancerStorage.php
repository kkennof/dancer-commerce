<?php

namespace Drupal\dancer;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the entity storage for dancer.
 */
class DancerStorage extends SqlContentEntityStorage {

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByUser(AccountInterface $account, $dancer_type_id, $published = TRUE) {
    $query = $this->getQuery();
    $query
      ->condition('uid', $account->id())
      ->sort('firstname', 'ASC')
      ->accessCheck(FALSE);
    $result = $query->execute();

    return $result ? $this->loadMultiple($result) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function loadByUser(AccountInterface $account, $dancer_type_id) {
    $query = $this->getQuery();
    $query
      ->condition('uid', $account->id())
      ->sort('firstname', 'ASC')
      ->range(0, 1)
      ->accessCheck(FALSE);
    $result = $query->execute();

    return $result ? $this->load(reset($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadDefaultByUser(AccountInterface $account, $dancer_type_id) {
    $result = $this->loadByProperties([
      'uid' => $account->id(),
    ]);

    return !empty($result) ? reset($result) : NULL;
  }

}
