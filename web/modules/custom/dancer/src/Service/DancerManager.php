<?php

namespace Drupal\dancer\Service;

use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\dancer\DancerInterface;
use Drupal\user\UserInterface;

class DancerManager {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The dancer storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dancerStorage;

  /**
   * The dancer type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $dancerTypeStorage;

  /**
   * Constructs a new DancerManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeBundleInfo $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->dancerStorage = $entity_type_manager->getStorage('dancer');
  }


  /**
   * {@inheritdoc}
   */
  public function loadAll(UserInterface $customer, $dancer_type_id = 'default') {
    if ($customer->isAnonymous()) {
      return [];
    }

    $dancers = $this->dancerStorage->loadMultipleByUser($customer, $dancer_type_id);

    return $dancers;
  }

  /**
   * {@inheritdoc}
   */
  public function load(UserInterface $customer, $dancer_type_id = 'default') {
    if ($customer->isAnonymous()) {
      return NULL;
    }

    $dancer = $this->dancerStorage->loadByUser($customer, $dancer_type_id);

    return $dancer;
  }

  /**
   * {@inheritdoc}
   */
  public function needsCopy(DancerInterface $dancer) {
    return (bool) $dancer->getData('copy_to_dancer_manager', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function copy(DancerInterface $dancer, UserInterface $customer) {
    if ($customer->isAnonymous()) {
      return;
    }

    // Check if there is an existing dancer manager dancer to update.
    // This can happen in two scenarios:
    // 1) The dancer was already copied to the dancer manager once.
    // 2) The customer is only allowed to have a single dancer manager dancer.
    $dancer_manager_dancer = NULL;
    $dancer_manager_dancer_id = $dancer->getData('dancer_manager_dancer_id');
    if ($dancer_manager_dancer_id) {
      /** @var \Drupal\dancer\DancerInterface $dancer_manager_dancer */
      $dancer_manager_dancer = $this->dancerStorage->load($dancer_manager_dancer_id);
    }

    $allow_multiple = TRUE;
    if (!$dancer_manager_dancer && !$allow_multiple) {
      $dancer_manager_dancer = $this->load($customer, $dancer->bundle());
    }

    if ($dancer_manager_dancer) {
      $dancer_manager_dancer->populateFromDancerEntity($dancer);
      $dancer_manager_dancer->save();
    }
    else {
      $dancer_manager_dancer = $dancer->createDuplicate();
      $dancer_manager_dancer->setOwnerId($customer->id());
      $dancer_manager_dancer->unsetData('copy_to_dancer_manager');
      $dancer_manager_dancer->save();
    }

    $dancer->unsetData('copy_to_dancer_manager');
    $dancer->setData('dancer_manager_dancer_id', $dancer_manager_dancer->id());
    $dancer->save();
  }
}
