<?php

namespace Drupal\dancer\Plugin\Commerce\InlineForm;

use Drupal\commerce\EntityHelper;
use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dancer\DancerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for managing a dancer.
 *
 * @CommerceInlineForm(
 *   id = "dancer",
 *   label = @Translation("Dancer"),
 * )
 */
class Dancer extends EntityInlineFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The dancer entity.
   *
   * @var \Drupal\dancer\DancerInterface
   */
  protected $entity;

  protected $dancerManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    // @todo: Define this service somewhere in the "dancer" module.
    $instance->dancerManager = $container->get('dancer.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'foo' => 'bar',
      // The uid of the customer whose dancer manager will be used.
      'dancer_manager_uid' => 0,
      // Whether dancer entity should be copied to the dancer manager after saving.
      // Pass FALSE if copying is done at a later point (e.g. order placement).
      'copy_on_save' => TRUE,
      // Whether the custom entity is being managed by an administrator.
      'admin' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function requiredConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    parent::validateConfiguration();

    if (empty($this->configuration['dancer_manager_uid'])) {
      // Defer copying if the customer is still unknown.
      $this->configuration['copy_on_save'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    // Do Logic.
    assert($this->entity instanceof DancerInterface);
    $allows_multiple = TRUE;
    $customer = $this->loadUser($this->configuration['dancer_manager_uid']);
    $dancer_manager_dancer = NULL;

    if ($customer->isAuthenticated() && $allows_multiple) {
      // Get an array of existing dancers for this user.
      $dancer_manager_dancers = $this->dancerManager->loadAll($customer);

      if ($dancer_manager_dancers) {
        $user_input = (array) NestedArray::getValue($form_state->getUserInput(), $inline_form['#parents']);

        // Determine the currently-displayed dancer entity.
        if (!empty($user_input['select_dancer'])) {
          // An option was selected, pre-fill the dancer form.
          $dancer_manager_dancer = $this->getDancerForOption($user_input['select_dancer'], $dancer_manager_dancers);

          $this->setEntity($dancer_manager_dancer);
        }
      }

      // Build a list of dropdown options from existing dancers.
      $dancer_options = $this->buildOptions($dancer_manager_dancers);
      if (!$this->entity->isNew()) {
        $selected_option = $this->selectOptionForDancer($this->entity);
      }
      else {
        $selected_option = '_new';
      }

      // Clear input values after changing the selected value.
      $inline_form['#after_build'][] = [get_called_class(), 'clearValues'];

      // Show dropdown.
      $inline_form['select_dancer'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a dancer'),
        '#options' => $dancer_options,
        '#default_value' => $selected_option,
        '#access' => !empty($dancer_manager_dancers),
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxRefresh'],
          'wrapper' => $inline_form['#id'],
        ],
        '#attributes' => [
          'class' => ['available-dancers'],
        ],
        '#weight' => -999,
      ];
    }

    // Show rendered dancer or new dancer form.
    if ($this->shouldRender($inline_form, $form_state)) {
      $view_builder = $this->entityTypeManager->getViewBuilder('dancer');
      $inline_form['rendered'] = $view_builder->view($this->entity);
      $inline_form['edit_button'] = [
        '#type' => 'button',
        '#name' => 'dancer_edit',
        '#value' => $this->t('Edit'),
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxRefresh'],
          'wrapper' => $inline_form['#id'],
        ],
        '#limit_validation_errors' => [$inline_form['#parents']],
        '#attributes' => [
          'class' => ['dancer-manager-edit-button'],
        ],
      ];
    }
    else {
      // Show new dancer form.
      $form_display = $this->loadFormDisplay();
      $form_display->buildForm($this->entity, $inline_form, $form_state);
    }

    return $inline_form;
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $inline_form = NestedArray::getValue($form, array_slice($triggering_element['#array_parents'], 0, -1));
    return $inline_form;
  }

  /**
   * Clears form input when select_dancer is used.
   */
  public static function clearValues(array $element, FormStateInterface $form_state) {
    $triggering_element_name = static::getTriggeringElementName($element, $form_state);
    if ($triggering_element_name != 'select_dancer') {
      return $element;
    }
    $user_input = &$form_state->getUserInput();
    $inline_form_input = NestedArray::getValue($user_input, $element['#parents']);
    $inline_form_input = array_intersect_assoc($inline_form_input, ['select_dancer' => $inline_form_input['select_dancer']]);
    NestedArray::setValue($user_input, $element['#parents'], $inline_form_input);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::validateInlineForm($inline_form, $form_state);

    if (!isset($inline_form['rendered'])) {
      $form_display = $this->loadFormDisplay();
      $form_display->extractFormValues($this->entity, $inline_form, $form_state);
      $form_display->validateFormValues($this->entity, $inline_form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);

    $this->entity->save();
  }

  /**
   * Loads a user entity for the given user ID.
   *
   * Falls back to the anonymous user if the user ID is empty or unknown.
   *
   * @param string $uid
   *   The user ID.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity.
   */
  protected function loadUser($uid) {
    $customer = User::getAnonymousUser();
    if (!empty($uid)) {
      $user_storage = $this->entityTypeManager->getStorage('user');
      /** @var \Drupal\user\UserInterface $user */
      $user = $user_storage->load($uid);
      if ($user) {
        $customer = $user;
      }
    }

    return $customer;
  }

  /**
   * Loads the form display used to build the dancer form.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   *   The form display.
   */
  protected function loadFormDisplay() {
    $form_mode = 'default';
    $form_display = EntityFormDisplay::collectRenderDisplay($this->entity, $form_mode);
    // The log message field should never be shown to customers.
    $form_display->removeComponent('revision_log_message');

    return $form_display;
  }

  /**
   * Determines whether the current dancer entity should be shown rendered.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the complete form.
   *
   * @return bool
   *   TRUE if the dancer should be shown rendered, FALSE otherwise.
   */
  protected function shouldRender(array $inline_form, FormStateInterface $form_state) {
    $render_parents = array_merge($inline_form['#parents'], ['render']);
    $triggering_element_name = static::getTriggeringElementName($inline_form, $form_state);
    if ($triggering_element_name == 'select_dancer') {
      // Reset the render flag to re-evaluate the newly selected dancer.
      $form_state->set($render_parents, NULL);
    }
    elseif ($triggering_element_name == 'edit_button') {
      // The edit button was clicked, turn off dancer rendering.
      $form_state->set($render_parents, FALSE);
    }

    $render = $form_state->get($render_parents);
    if (!isset($render)) {
      $render = !$this->isDancerIncomplete($this->entity);
      $form_state->set($render_parents, $render);
    }

    return $render;
  }


  /**
   * Builds the list of options for the given dancer manager dancers.
   *
   * Adds the special _original and _new options.
   *
   * @param \Drupal\dancer\DancerInterface[] $dancer_manager_dancers
   *   The dancer manager dancers.
   *
   * @return array
   *   The dancer options.
   */
  protected function buildOptions(array $dancer_manager_dancers) {
    $dancer_options = EntityHelper::extractLabels($dancer_manager_dancers);
    // The customer dancer entity is not new, indicating that it is being edited.
    // Add an _original option to allow the customer to revert their changes
    // after selecting a different option.
    $dancer_options['_new'] = $this->t('+ Enter a new dancer');

    return $dancer_options;
  }

  /**
   * Selects the option ID for the given dancer.
   *
   * @param \Drupal\dancer\DancerInterface $dancer
   *   The dancer manager dancer.
   *
   * @return string
   *   The option ID. A dancer ID, or '_new'.
   */
  protected function selectOptionForDancer(DancerInterface $dancer) {
    if ($dancer->isNew()) {
      $option_id = '_new';
    }
    else {
      $option_id = $dancer->id();
    }

    return $option_id;
  }

  /**
   * Gets the dancer manager dancer for the given option ID.
   *
   * @param string $option_id
   *   The option ID. A dancer ID, or a special value ('_original', '_new').
   * @param \Drupal\dancer\DancerInterface[] $dancer_manager_dancers
   *   The dancer manager dancers.
   *
   * @return \Drupal\dancer\DancerInterface|null
   *   The dancer, or NULL if none found.
   */
  protected function getDancerForOption($option_id, array $dancer_manager_dancers) {
    $dancer_storage = $this->entityTypeManager->getStorage('dancer');
    /** @var \Drupal\dancer\DancerInterface $dancer_manager_dancer */
    if ($option_id == '_new') {
      $dancer = $dancer_storage->create([
        'type' => $this->entity->bundle(),
        'uid' => $this->entity->getOwnerId(),
      ]);
    }
    else {
      assert(is_numeric($option_id));
      $dancer = $dancer_manager_dancers[$option_id] ?? $this->selectDefaultDancer($dancer_manager_dancers);
    }

    return $dancer;
  }

  /**
   * Selects a default dancer from the given set of dancer manager dancers.
   *
   * @param \Drupal\dancer\DancerInterface[] $dancer_manager_dancers
   *   The dancer entities.
   *
   * @return \Drupal\dancer\DancerInterface|false
   *   The selected default dancer, or FALSE if no dancers were given.
   */
  protected function selectDefaultDancer(array $dancer_manager_dancers) {
    $default_dancer = reset($dancer_manager_dancers);

    return $default_dancer;
  }

  /**
   * Checks whether the given dancer entity is incomplete.
   *
   * A dancer entity is incomplete if it has an empty required field.
   *
   * @param \Drupal\dancer\DancerInterface $dancer
   *   The dancer.
   *
   * @return bool
   *   TRUE if the given dancer is incomplete, FALSE otherwise.
   */
  protected function isDancerIncomplete(DancerInterface $dancer) {
    $violations = $dancer->validate();

    return count($violations) > 0;
  }

  /**
   * Gets the copy label.
   *
   * @param bool $update_on_copy
   *   Whether the copy will update an existing dancer manager dancer.
   *
   * @return string
   *   The copy label.
   */
  protected function getCopyLabel($update_on_copy) {
    $is_owner = FALSE;
    if (!$this->configuration['admin']) {
      $is_owner = \Drupal::currentUser()->id() == $this->configuration['dancer_manager_uid'];
    }

    if ($is_owner) {
      if ($update_on_copy) {
        $copy_label = $this->t('Also update the dancer in my dancer manager.');
      }
      else {
        $copy_label = $this->t('Save to my dancer manager.');
      }
    }
    else {
      if ($update_on_copy) {
        $copy_label = $this->t("Also update the dancer in the customer's dancer manager.");
      }
      else {
        $copy_label = $this->t("Save to the customer's dancer manager.");
      }
    }

    return $copy_label;
  }

  /**
   * Determines the name of the triggering element.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the complete form.
   *
   * @return string
   *   The name of the triggering element, if the triggering element is
   *   a part of the inline form.
   */
  protected static function getTriggeringElementName(array $inline_form, FormStateInterface $form_state) {
    $triggering_element_name = '';
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element) {
      $parents = array_slice($triggering_element['#parents'], 0, count($inline_form['#parents']));
      if ($inline_form['#parents'] === $parents) {
        $triggering_element_name = end($triggering_element['#parents']);
      }
    }

    return $triggering_element_name;
  }
}
