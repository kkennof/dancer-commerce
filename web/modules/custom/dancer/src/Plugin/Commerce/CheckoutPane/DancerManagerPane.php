<?php

namespace Drupal\dancer\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "my_checkout_pane_dancer_manager",
 *   label = @Translation("Dancer Manager"),
 *   display_label = @Translation("Dancer information"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class DancerManagerPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  protected $dancerManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager')
    );

    $instance->inlineFormManager = $container->get('plugin.manager.commerce_inline_form');
    $instance->dancerManager = $container->get('dancer.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $summary = [];
    // Render our custom entity in the "default" view_mode for the Checkout Review summary.
    if (!$this->order->get('dancer_information')->isEmpty()) {
      $dancer = $this->order->get('dancer_information')->entity;

      $dancer_view_builder = $this->entityTypeManager->getViewBuilder('dancer');
      $summary = $dancer_view_builder->view($dancer, 'default');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Load or create a "custom entity" object.
    if (!$this->order->get('dancer_information')->isEmpty()) {
      $dancer = $this->order->get('dancer_information')->entity;
    }
    else {
      // Get an array of existing dancers for this user.
      $dancer_manager_dancers = $this->dancerManager->loadAll($this->order->getCustomer());

      if (!empty($dancer_manager_dancers)) {
        // Use the default dancer to pre-fill the dancer form.
        $dancer = reset($dancer_manager_dancers);
      }
      else {
        $dancer_storage = $this->entityTypeManager->getStorage('dancer');
        $dancer = $dancer_storage->create([
          'type' => 'dancer',
          'uid' => $this->order->getCustomerId(),
          // other fields.
        ]);
      }
    }

    $inline_form = $this->inlineFormManager->createInstance('dancer', [
      // Pass along some options.
      'foo' => 'bar',
      // Pass along the order's owner UID.
      'dancer_manager_uid' => $this->order->getCustomerId(),
      // Don't copy the profile to address book until the order is placed.
      'copy_on_save' => FALSE,
    ], $dancer);

    $pane_form['dancer'] = [
      '#parents' => array_merge($pane_form['#parents'], ['dancer']),
      '#inline_form' => $inline_form,
    ];
    $pane_form['dancer'] = $inline_form->buildInlineForm($pane_form['dancer'], $form_state);

    // The dancer should always exist in form state.
    if (!$form_state->has('dancer') ||
      (isset($triggering_element['#parents']) && in_array('select_dancer', $triggering_element['#parents']))) {
      $form_state->set('dancer', $inline_form->getEntity());
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Extract our dancer back form our inline-form plugin and set it to our order.
    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
    $inline_form = $pane_form['dancer']['#inline_form'];
    $dancer = $inline_form->getEntity();

    $this->order->set('dancer_information', $dancer);
  }

}
