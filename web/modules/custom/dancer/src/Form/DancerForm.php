<?php

namespace Drupal\dancer\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the dancer entity edit forms.
 */
class DancerForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();
    $dancer_fullname = $entity->firstname->value . ' ' . $entity->name->value;

    $message_arguments = ['%label' => $dancer_fullname];
    $logger_arguments = [
      '%label' => $dancer_fullname,
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New dancer %label has been created.', $message_arguments));
        $this->logger('dancer')->notice('Created new dancer %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The dancer %label has been updated.', $message_arguments));
        $this->logger('dancer')->notice('Updated dancer %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.dancer.canonical', ['dancer' => $entity->id()]);

    return $result;
  }

}
