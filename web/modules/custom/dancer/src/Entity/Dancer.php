<?php
namespace Drupal\dancer\Entity;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\dancer\DancerInterface;
use Drupal\user\EntityOwnerTrait;
/**
 * Defines the dancer entity class.
 *
 * @ContentEntityType(
 *   id = "dancer",
 *   label = @Translation("Dancer"),
 *   label_collection = @Translation("Dancers"),
 *   label_singular = @Translation("dancer"),
 *   label_plural = @Translation("dancers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count dancers",
 *     plural = "@count dancers",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\dancer\DancerStorage",
 *     "list_builder" = "Drupal\dancer\DancerListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\dancer\Form\DancerForm",
 *       "edit" = "Drupal\dancer\Form\DancerForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "dancer",
 *   admin_permission = "administer dancer",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "firstname",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/dancer",
 *     "add-form" = "/dancer/add",
 *     "canonical" = "/dancer/{dancer}",
 *     "edit-form" = "/dancer/{dancer}/edit",
 *     "delete-form" = "/dancer/{dancer}/delete",
 *   },
 *   field_ui_base_route = "entity.dancer.settings",
 * )
 */
class Dancer extends ContentEntityBase implements DancerInterface {
  use EntityChangedTrait;
  use EntityOwnerTrait;
  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('ID of the dancer'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('UUID of the dancer'))
      ->setReadOnly(TRUE);

    $fields['firstname'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First name of the dancer'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last name of the dancer'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['sex'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Sex of the dancer'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'allowed_values' => array(
          'M' => t('Male'),
          'V' => t('Female'),
          'A' => t('Other'),
          'Z' => t('Rather not say it')
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'list_default',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['birthdate'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Birthdate of the dancer'))
      ->setRequired(TRUE)
      ->setSettings([
        'datetime_type' => 'date'
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form',TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('E-mail of the dancer'))
      ->setDescription(t('If the dancer has an e-mailaddress, you can fill it out here so he/she keeps updated about MADE2MOVE.'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'basic_string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['telephone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Telephone number of the dancer'))
      ->setDescription((t('If the dancer has a telephone number, you can fill it out here so he/she keeps updated about MADE2MOVE.')))
      ->setDefaultValue('')
      ->setDisplayOptions('form', array(
        'type' => 'telephone_default',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent of the dancer'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner');

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'))
      ->setRevisionable(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the dancer was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the dancer was last edited.'));

    return $fields;
  }
  /**
   * {@inheritdoc}
   */
  public function getData($key, $default = NULL) {
    $data = [];
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
    }
    return isset($data[$key]) ? $data[$key] : $default;
  }
  /**
   * {@inheritdoc}
   */
  public function setData($key, $value) {
    $this->get('data')->__set($key, $value);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function unsetData($key) {
    if (!$this->get('data')->isEmpty()) {
      $data = $this->get('data')->first()->getValue();
      unset($data[$key]);
      $this->set('data', $data);
    }
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function equalToDancerEntity(DancerInterface $dancer, array $field_names = []) {
    // Compare all configurable fields by default.
    $field_names = $field_names ?: $this->getConfigurableFieldNames($dancer);
    foreach ($field_names as $field_name) {
      $profile_field_item_list = $dancer->get($field_name);
      if (!$this->hasField($field_name) || !$this->get($field_name)->equals($profile_field_item_list)) {
        return FALSE;
      }
    }
    return TRUE;
  }
  /**
   * {@inheritdoc}
   */
  public function populateFromDancerEntity(DancerInterface $dancer, array $field_names = []) {
    // Transfer all configurable fields by default.
    $field_names = $field_names ?: $this->getConfigurableFieldNames($dancer);
    $dancer_values = $dancer->toArray();
    foreach ($field_names as $field_name) {
      if (isset($dancer_values[$field_name]) && $this->hasField($field_name)) {
        $this->set($field_name, $dancer_values[$field_name]);
      }
    }
    return $this;
  }
  /**
   * Gets the names of all configurable fields on the given dancer.
   *
   * @param \Drupal\dancer\DancerInterface $dancer
   *   The dancer.
   *
   * @return string[]
   *   The field names.
   */
  protected function getConfigurableFieldNames(DancerInterface $dancer) {
    $field_names = [];
    foreach ($dancer->getFieldDefinitions() as $field_name => $definition) {
      if (!($definition instanceof BaseFieldDefinition)) {
        $field_names[] = $field_name;
      }
    }
    return $field_names;
  }
}
