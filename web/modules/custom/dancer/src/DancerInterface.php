<?php

namespace Drupal\dancer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a dancer entity type.
 */
interface DancerInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
